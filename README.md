Ingénieur d'étude dans l'unité [MIAT](https://miat.inrae.fr) au sein de l'équipe [RECORD](https://eq-record.pages.mia.inra.fr/record-team/)

## Outils et langages : 
<a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/linux/linux-icon.svg" alt="linux" width="40" height="40"/> </a> 
<a href="https://www.gnu.org/software/bash/" target="_blank" rel="noreferrer"> <img src="https://bashlogo.com/img/symbol/svg/full_colored_dark.svg" alt="bash" width="40" height="40" /> </a> 
<a href="https://www.virtualbox.org/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/virtualbox/virtualbox-icon.svg" alt="virtualbox" width="40" height="40"/> </a> 
<a href="https://slurm.schedmd.com/" target="_blank" rel="noreferrer"> <img src="https://slurm.schedmd.com/slurm_logo.png" alt="slurm" width="40" height="40"/> </a> 

<a href="https://about.gitlab.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/gitlab/gitlab-icon.svg" alt="gitlab" width="40" height="40"/> </a> 
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> 
<a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/docker/docker-icon.svg" alt="docker" width="40" height="40"/> </a> 

<a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a> 
<a href="https://cmake.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/cmake/cmake-icon.svg" alt="cmake" width="40" height="40"/> </a> 
<a href="https://www.vle-project.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/vle-forge/vle/master/share/pixmaps/yellowvle.svg" alt="vle" width="40" height="40"/> </a> 

<a href="https://cran.r-project.org/" target="_blank" rel="noreferrer"> <img src="https://cran.r-project.org/Rlogo.svg" alt="R-cran" width="40" height="40"/> </a> 
<a href="https://www.rstudio.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/rstudio/hex-stickers/master/SVG/RStudio.svg" alt="Rstudio" width="40" height="40"/> </a> 
<a href="https://rmarkdown.rstudio.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/rstudio/hex-stickers/master/SVG/rmarkdown.svg" alt="Rstudio" width="40" height="40"/> </a> 
<a href="https://shiny.rstudio.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/rstudio/hex-stickers/master/SVG/shiny.svg" alt="shiny" width="40" height="40"/> </a> 
<a href="https://sk8.inrae.fr/" target="_blank" rel="noreferrer"> <img src="https://sk8.inrae.fr/images/SK8-logo-trans-128.png" alt="sk8" width="40" height="40"/> </a> 

<a href="https://fortran-lang.org" target="_blank" rel="noreferrer"> <img src="https://fortran-lang.org/en/_static/fortran-logo-256x256.png" alt="fortran" width="40" height="40"/> </a> 
<a href="https://www6.paca.inrae.fr/stics/" target="_blank" rel="noreferrer"> <img src="https://stics.paca.hub.inrae.fr/var/hub_internet_paca_agroclim_stics/storage/images/_aliases/inra_logo/configuration-graphique/haut/logo_stics/29968-1-fre-FR/Logo_stics.png" alt="stics" width="40" height="40"/> </a> 

<a href="https://www.markdownguide.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/dcurtis/markdown-mark/master/svg/markdown-mark-solid.svg" alt="markdown" width="40" height="40"/> </a> 

## Liens perso : 

* [Docker & Gitlab-CI](https://record.pages.mia.inra.fr/gitlab-ci-cd-and-docker/)
